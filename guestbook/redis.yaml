#  A Deployment in Kubernetes manages a set of processes, allowing you
#  scale them up and reconfigure them over time.
#
#  This Deployment will run a Redis primary instance.
apiVersion: apps/v1
kind: Deployment
metadata:
  name: redis-master
  namespace: guestbook
spec:
  selector:
    matchLabels:
      app: redis
      role: primary
      tier: backend
  replicas: 1
  template:
    metadata:
      labels:
        app: redis
        role: primary
        tier: backend
    spec:
      containers:
      - name: primary
        image: k8s.gcr.io/redis:e2e
        resources:
          requests:
            cpu: 100m
            memory: 100Mi
        ports:
        - containerPort: 6379
---
#  A service is the equivalent of an internal load-balancer inside Kubernetes.
#
#  This service exposes the Redis primary instance inside the cluster.
apiVersion: v1
kind: Service
metadata:
  name: redis-master
  namespace: guestbook
  labels:
    app: redis
    role: primary
    tier: backend
spec:
  ports:
  - port: 6379
    targetPort: 6379
  selector:
    app: redis
    role: primary
    tier: backend
---
#  This Deployment will run two Redis secondary instances.
apiVersion: apps/v1
kind: Deployment
metadata:
  name: redis-slave
  namespace: guestbook
spec:
  selector:
    matchLabels:
      app: redis
      role: secondary
      tier: backend
  replicas: 2
  template:
    metadata:
      labels:
        app: redis
        role: secondary
        tier: backend
    spec:
      containers:
      - name: secondary
        image: gcr.io/google_samples/gb-redisslave:v1
        resources:
          requests:
            cpu: 100m
            memory: 100Mi
        env:
        - name: GET_HOSTS_FROM
          value: dns
        ports:
        - containerPort: 6379
---
#  This service exposes the Redis secondary instances inside the cluster.
apiVersion: v1
kind: Service
metadata:
  name: redis-slave
  namespace: guestbook
  labels:
    app: redis
    role: secondary
    tier: backend
spec:
  ports:
  - port: 6379
  selector:
    app: redis
    role: secondary
    tier: backend
