Kubernetes Local Sandbox and Demo
=================================

This repository helps you spin up a local copy of Kubernetes and deploy a sample
application to it.  

The goals of this are:
- provide a localized Kubernetes implementation for practice and development
- deploy an application to demonstrate basic functionality
- experience with the basics of the Kubernetes CLI
- show off some compelling features, by adding monitoring of the deployed application.

The sandbox Kubernetes provided by this demo uses [K3S by Rancher](https://k3s.io), which is a
lightweight implementation ideal for running on a laptop with minimal memory and CPU requirements.

Install Multipass
-----------------

You will be running Kubernetes inside of an Ubuntu instance using [Multipass](https://multipass.run).

Visit [multipass.run](https://multipass.run), download the appropriate version for your
operating system, and install it.

Clone this repository
---------------------

Using git, clone this repository:

```bash
$ git clone git@gitlab.com:aarnvns/local-kubernetes-demo-k3s.git
$ cd local-kubernetes-demo-k3s
```

Spin Up Local Kubernetes
------------------------

### Create Multipass Ubuntu instance

This repository contains a [utility script](launch-multipass.sh) to create the multipass VM for you.

```bash
$ ./launch-multipass.sh 
Launched: k3s                                                                   
```

The utility script will create an appropriately sized Ubuntu instance, and mount the current directory
into it under the path `/demo`.

### Note the IP address of your Ubuntu instance

In order to load the sample web application in your browser later, you'll need the IP address of the
Multipass instance.

In the following example, my VM has the IP address `192.168.64.8`.

```bash
$ multipass list
Name                    State             IPv4             Image
k3s                     Running           192.168.64.8     Ubuntu 18.04 LTS
```

### Attach to Ubuntu

Connect to the Ubuntu VM:

```bash
$ multipass shell k3s
Welcome to Ubuntu 18.04.4 LTS (GNU/Linux 4.15.0-96-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Thu Apr 16 18:37:10 CDT 2020

  System load:  0.79              Processes:             141
  Usage of /:   19.4% of 9.52GB   Users logged in:       0
  Memory usage: 36%               IP address for enp0s2: 192.168.64.8
  Swap usage:   0%                IP address for cni0:   10.42.0.1


4 packages can be updated.
2 updates are security updates.

ubuntu@k3s:~$ 
```

Test out that everything is running properly, by using the `kubectl` command to list
the nodes in the cluster along with the currently running pods:

```bash
ubuntu@k3s:~$ kubectl get nodes
NAME   STATUS   ROLES    AGE    VERSION
k3s    Ready    master   4m3s   v1.17.4+k3s1

ubuntu@k3s:~$ kubectl get pods --all-namespaces
NAMESPACE     NAME                                      READY   STATUS      RESTARTS   AGE
kube-system   local-path-provisioner-58fb86bdfd-rltqk   1/1     Running     0          3m24s
kube-system   metrics-server-6d684c7b5-5jb6l            1/1     Running     0          3m24s
kube-system   coredns-6c6bb68b64-hz29d                  1/1     Running     0          3m24s
kube-system   helm-install-traefik-qss4l                0/1     Completed   0          3m25s
kube-system   svclb-traefik-gbbjl                       2/2     Running     0          2m41s
kube-system   traefik-7b8b884c8-m5tzs                   1/1     Running     0          2m41s
```

### Change to the /demo directory

After spinning up the VM, the contents of this repository should be located under `/demo` in the VM:

```bash
ubuntu@k3s:~$ cd /demo
ubuntu@k3s:/demo$ 
```

Deploy Guestbook Application
----------------------------

We'll be deploying a simple Guestbook demo application.  This application has multiple components:
- guestbook application
- redis primary
- redis secondary

First create the namespaces that will be used in this demo:

```bash
ubuntu@k3s:/demo$ kubectl apply -f namespaces.yaml 
namespace/monitoring created
namespace/guestbook created
```

No we can deploy the guestbook application to one of those namespaces:

```bash
ubuntu@k3s:/demo$ kubectl apply -f guestbook/redis.yaml && \
> kubectl apply -f guestbook/guestbook.yaml

deployment.apps/redis-master created
service/redis-master created
deployment.apps/redis-slave created
service/redis-slave created
deployment.apps/guestbook created
service/guestbook created
ingress.extensions/guestbook created
```

You might want to wait a second while all of the pods for this application start.

Using the `--watch` flag with `kubectl -n guestbook get pods` allows us to observe changes
to the state of the pods in the `guestbook` namespace:

```bash
ubuntu@k3s:/demo$ kubectl -n guestbook get pods --watch

NAME                           READY   STATUS              RESTARTS   AGE
redis-slave-69cf4cbb8f-rjk47   0/1     ContainerCreating   0          45s
redis-master-dc4b59fdc-tfgm4   0/1     ContainerCreating   0          45s
redis-slave-69cf4cbb8f-knq9q   0/1     ContainerCreating   0          45s
guestbook-9fcfcc7d5-2jhlp      0/1     ContainerCreating   0          44s
guestbook-9fcfcc7d5-phwzd      0/1     ContainerCreating   0          44s
guestbook-9fcfcc7d5-c5bcw      0/1     ContainerCreating   0          44s
redis-slave-69cf4cbb8f-rjk47   1/1     Running             0          66s
redis-slave-69cf4cbb8f-knq9q   1/1     Running             0          66s
redis-master-dc4b59fdc-tfgm4   1/1     Running             0          93s
guestbook-9fcfcc7d5-c5bcw      1/1     Running             0          97s
guestbook-9fcfcc7d5-phwzd      1/1     Running             0          97s
guestbook-9fcfcc7d5-2jhlp      1/1     Running             0          97s
^C
```

Access Guestbook Application
----------------------------

Earlier you made a note of the IP address of your Ubuntu VM where Kubernetes is running.
In my case, the IP address is `192.168.64.8`.  Once all the pods are started, the guestbook
is accessible at `http://192.168.64.8/guestbook`.

Deploy Prometheus and Grafana
-----------------------------

Now we want to start monitoring applications within the cluster and the cluster itself.
To do that, you can install Prometheus and Grafana.

*(Note: you can ignore the warnings about "crd-install")*

```bash
ubuntu@k3s:/demo$ helm repo add stable https://kubernetes-charts.storage.googleapis.com

"stable" has been added to your repositories
ubuntu@k3s:/demo$ helm repo update
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "stable" chart repository
Update Complete. ⎈ Happy Helming!⎈ 
ubuntu@k3s:/demo$ helm install prometheus-operator stable/prometheus-operator -n monitoring -f prometheus/helm-config.yaml
manifest_sorter.go:192: info: skipping unknown hook: "crd-install"
manifest_sorter.go:192: info: skipping unknown hook: "crd-install"
manifest_sorter.go:192: info: skipping unknown hook: "crd-install"
manifest_sorter.go:192: info: skipping unknown hook: "crd-install"
manifest_sorter.go:192: info: skipping unknown hook: "crd-install"
manifest_sorter.go:192: info: skipping unknown hook: "crd-install"
NAME: prometheus-operator
LAST DEPLOYED: Fri Apr 17 09:49:15 2020
NAMESPACE: monitoring
STATUS: deployed
REVISION: 1
NOTES:
The Prometheus Operator has been installed. Check its status by running:
  kubectl --namespace monitoring get pods -l "release=prometheus-operator"

Visit https://github.com/coreos/prometheus-operator for instructions on how
to create & configure Alertmanager and Prometheus instances using the Operator.
```

In order to access Prometheus and Grafana with your browser, install the following
ingress.  An `ingress` is roughly similar to nginx.conf or apache.conf, configuring
a reverse proxy to route certain HTTP requests based on the URL.

```bash
ubuntu@k3s:/demo$ kubectl apply -f prometheus/ingress.yaml 
ingress.extensions/prometheus created
```

Make sure all the pods are running:

```bash
ubuntu@k3s:/demo$ kubectl -n monitoring get pods
NAME                                                      READY   STATUS    RESTARTS   AGE
prometheus-operator-kube-state-metrics-868fb5d6d4-rrfvj   1/1     Running   0          3m40s
prometheus-operator-prometheus-node-exporter-j5qtc        1/1     Running   0          3m40s
prometheus-operator-operator-7df846c57d-scs9s             2/2     Running   0          3m40s
alertmanager-prometheus-operator-alertmanager-0           2/2     Running   0          3m21s
prometheus-operator-grafana-79656f4554-jltbn              2/2     Running   0          3m40s
prometheus-prometheus-operator-prometheus-0               3/3     Running   1          3m10s
```

You should now be able to access Prometheus at `http://192.168.64.8/prometheus/graph` (change the IP address to match your VM).

Grafana is located at `http://192.168.64.8/grafana/`.

Monitor Guestbook Application
-----------------------------

If you explore Grafana, you will find a number of dashboards showing you the health and performance of your
Kubernetes cluster.  We'd like to specifically monitor the guestbook application, so in order to do that
we'll create a `ServiceMonitor` telling Prometheus to monitor the application and a custom dashboard that will
appear in Grafana:

```bash
ubuntu@k3s:/demo$ kubectl apply -f prometheus/traefik-servicemonitor.yaml 
servicemonitor.monitoring.coreos.com/traefik created
ubuntu@k3s:/demo$ kubectl apply -f guestbook/dashboard.yaml 
configmap/traefik-ingress created
```

Now that we're monitoring the Guestbook, return to `http://192.168.64.8/guestbook` and refresh the
page several times.  Also try hitting `http://192.168.64.8/guestbook/badurl` a few times to generate
some 404 responses.

Now return to `http:/192.168.64.8/grafana/`, click "Home" in the top-right of the page, and type "Guestbook Monitoring"
where it says "Find dashboards by name".  You might need to refresh the page, since it takes Grafana
several seconds to locate newly uploaded dashboards.

You should now see a simple dashboard showing the request counts summed by HTTP code, and the
latency of the requests.

Summary
-------

In this demo, you spun up a local copy of Kubernetes, deployed an application to it, and
then deployed monitoring to the cluster.

You were also exposed to `kubectl` which is a powerful CLI for interacting with your
cluster and the applications running inside it.

The power of Kuberenetes is its API, and the ability to customize the cluster
to suit your needs.

What you've seen here can be expanded upon to build a platform where applications
are being continuously deployed to the cluster via CI/CD, ubiquitous monitoring
is the norm, and you have a simple API tying it all together.

Thanks for reading!
