#!/bin/sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

multipass launch --name k3s --cloud-init ${DIR}/multipass/cloud-init.yaml --mem 2G --disk 10G
multipass mount "${DIR}" k3s:/demo
